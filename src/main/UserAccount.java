package main;

public class UserAccount {
    private String firstname;
    private String secondname;
    private String surname;
    private String username;
    private String email;
    private String age;

    private UserAccount(){}

    public String getFirstName() {
        return firstname;
    }

    public String getSecondName() {
        return secondname;
    }

    public String getSurName() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getAge() {
        return age;
    }

    public static Builder newBuilder(){
        return new UserAccount().new Builder();
    }

    public class Builder{

        private Builder(){}

        public Builder setFirstname(String firstname){
            UserAccount.this.firstname = firstname;
            return this;
        }

        public Builder setSecondname(String secondname){
            UserAccount.this.secondname = secondname;
            return this;
        }

        public Builder setSurname(String surname){
            UserAccount.this.surname = surname;
            return this;
        }

        public Builder setUsername(String username){
            UserAccount.this.username = username;
            return this;
        }

        public Builder setEmail(String email){
            UserAccount.this.email = email;
            return this;
        }

        public Builder setAge(String age){
            UserAccount.this.age = age;
            return this;
        }

        public UserAccount build(){
            UserAccount userAccount = new UserAccount();
            userAccount.firstname = UserAccount.this.firstname;
            userAccount.secondname = UserAccount.this.secondname;
            userAccount.surname = UserAccount.this.surname;
            userAccount.username = UserAccount.this.username;
            userAccount.email = UserAccount.this.email;
            userAccount.age = UserAccount.this.age;

            return userAccount;
        }
    }
}
